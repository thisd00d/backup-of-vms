#!/bin/bash

# Define global variables
current_date=$(date +"%Y%m%d")
vm_directory="/mnt/vm-disk/"
backup_destination="/mnt/evoplus-disk/VMs and IOS/"

vm1="vm-name1"
vm2="vm-name2"
# vm3="vm-name3"

# Step 1: Shutdown 3 virtual machines using virsh
echo 'Step 1: Shutdown 3 virtual machines using virsh'
sudo virsh shutdown $vm1
sudo virsh shutdown $vm2
# sudo virsh shutdown $vm3

# Wait for VMs to shut down
echo 'Sleep for 30 seconds'
sleep 30

# Step 2: Copy the directory where the VMs are stored to a backup location
echo 'Step 2: Copy the directory where the VMs are stored to a backup location'
sudo cp -rv "${vm_directory}"$vm1/ "${backup_destination}"
sudo cp -rv "${vm_directory}"$vm2/ "${backup_destination}"
# sudo cp -rv "${vm_directory}"$vm3/ "${backup_destination}"

# Step 3: Change ownership of the new directories in the backup location
echo 'Step 3: Change ownership of the new directories in the backup location'
sudo chown -R user:group "${backup_destination}"$vm1/
sudo chown -R user:group "${backup_destination}"$vm2/
# sudo chown -R user:group "${backup_destination}"$vm3/

# Step 4: Rename the backups to include the date
echo 'Step 4: Rename the backups to include the date'
mv "${backup_destination}"$vm1/ "${backup_destination}""${vm1}"_${current_date}/
mv "${backup_destination}"$vm2/ "${backup_destination}""${vm2}"_${current_date}/
# mv "${backup_destination}"$vm3/ "${backup_destination}""${vm3}"_${current_date}/

# Step 5: Start the 3 virtual machines using virsh
echo 'Step 5: Start the 3 virtual machines using virsh'
sudo virsh start $vm1
sudo virsh start $vm2
# sudo virsh start $vm3

echo "Script end"