# Backup of VMs
The purpose of this bash script is to backup multiple VMs.
The script will first shutdown the VMs, then copy the VMs to another directory, and change the owner from root to the specified user.
When finished the VMs will be started again automatically.